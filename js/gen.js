var current_time = new Date();

function GenerateLink() {
    var custom_d = parseInt(document.getElementById('custom_day').value);
    var custom_m = parseInt(document.getElementById('custom_month').value);
    var custom_y = parseInt(document.getElementById('custom_year').value);
    var custom_h = parseInt(document.getElementById('custom_hour').value);
    var custom_mn = parseInt(document.getElementById('custom_min').value);
    var custom_s = parseInt(document.getElementById('custom_sec').value);
    var custom_text = document.getElementById('custom_txt').value;

    custom_text = encodeURIComponent(custom_text);

    if ( custom_text == "" ) {
        window.alert("Nazwa wydarzenia nie może być pusta!");
        return;
    } else if ( document.getElementById('custom_year').checkValidity() == false ) {
        window.alert("Rok musi być liczbą oraz nie może być mniejszy niż aktualny!");
        return;
    }

    document.getElementById('custom_link').value = "https://kiedywakacje.pl/odliczanie_custom.html?y=" + custom_y + "&m=" + custom_m + "&d=" + custom_d + "&h=" + custom_h + "&mn=" + custom_mn + "&s=" + custom_s + "&txt=" + custom_text;
    document.getElementById("custom_copy").disabled = false; 
    document.getElementById("custom_follow").disabled = false; 
}

function CopyLink() {
    var copyTextarea = document.getElementById('custom_link');
    copyTextarea.focus();
    copyTextarea.select();
    navigator.clipboard.writeText(copyTextarea.value);
}

function FollowLink() {
    var copyTextarea = document.getElementById('custom_link');
    window.open(copyTextarea.value);
}

window.addEventListener('load', function () {
    var fieldYear = document.getElementById('custom_year');
    
    document.getElementById('custom_submit').addEventListener('click', GenerateLink);
    document.getElementById('custom_copy').addEventListener('click', CopyLink);
    document.getElementById('custom_follow').addEventListener('click', FollowLink);

    fieldYear.min = current_time.getFullYear();
    fieldYear.value = current_time.getFullYear();
});