var konamiCode = '38,38,40,40,37,39,37,39,66,65';
var keyPresses = [];

var checkKonami = function (e) {
    keyPresses.push(e.keyCode);
    if (keyPresses.slice(keyPresses.length - 10).join() === konamiCode) {
        runKonami();
    }
}

var runKonami = function () {
    _paq.push(['trackEvent', 'Extras', 'Easter Egg', 'Konami code']);
    window.open("https://kiedywakacje.pl/files/file.jpg", "_self")
}

document.addEventListener('keyup', checkKonami);