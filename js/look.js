function setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/" + ";SameSite=Strict" + ";Secure";
}

function getCookie(cname) {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie(cname) {
    document.cookie = cname + "=;" + "expires=Thu, 01 Jan 1970 00:00:00 UTC" + ";path=/" + ";SameSite=Strict" + ";Secure";
}

function switchLook() {
    var dark_css = document.getElementById('dark_css');

    if (dark_css.rel == 'stylesheet alternate') {
        dark_css.rel = 'stylesheet';
        setCookie("look", "dark", 365);
        _paq.push(['trackEvent', 'Look', 'Color mode', 'Change to dark']);
    }
    else {
        dark_css.rel = 'stylesheet alternate';
        deleteCookie("look");
        _paq.push(['trackEvent', 'Look', 'Color mode', 'Change to light']);
    }
}

function turnDark() {
    var dark_css = document.getElementById('dark_css');

    dark_css.rel = 'stylesheet';
}

if (getCookie("look") == "dark") {
    turnDark();
    setCookie("look", "dark", 365);
}

window.addEventListener('load', function () {
    document.getElementById('dark').addEventListener('click', switchLook);
});