// Author: ricocheting.com
// Version: v2.0
// Date: 2011-03-31
// Description: displays the amount of time until the "dateFuture" entered below.
var dateFuture1 = new Date(2025, 0, 27, 0, 0, 0);

function Rzeczownik(val, p1, p2, p3) {
    if (val == 1) //dla jedynki
    {
        return p1;
    }
    if (((val % 10) == 2 || (val % 10) == 3 || (val % 10) == 4) && (val % 100) != 12 && (val % 100) != 13 && (val % 100) != 14) //2,3,4,22,23,24,32,33,34 itp. bez 12,13,14
    {
        return p2;
    }
    else //reszta
    {
        return p3;
    }
}

function GetCountTyg(ddate, iid) {
    var dateNow = new Date();	//grab current date
    var amount = ddate.getTime() - dateNow.getTime();	//calc milliseconds between dates
    delete dateNow;
    // if time is already past
    if (amount < 0) {
        document.getElementById(iid).innerHTML = "";
        var finished_note = document.getElementById("finished_note");
        var counters = document.getElementsByClassName("counter");
        var explain_main = document.getElementsByClassName("explain_main");
        var explain_small = document.getElementsByClassName("explain_small");
        for (var i = 0, len = counters.length; i < len; i++) {
            counters[i].style.display = "none";
        }
        for (var i = 0, len = explain_small.length; i < len; i++) {
            explain_small[i].style.display = "none";
        }
        for (var i = 0, len = explain_main.length; i < len; i++) {
            explain_main[i].style.display = "none";
        }
        finished_note.style.display = "inline";
    }
    // else date is still good
    else {
        var weeks = 0;
        var out = "";
        amount = Math.floor(amount / 1000);//kill the "milliseconds" so just secs
        weeks = Math.floor(amount / 604800);//weeks
        out += weeks + " " + Rzeczownik(weeks, "tydzień", "tygodnie", "tygodni");
        document.getElementById(iid).innerHTML = out;
        setTimeout(function () { GetCountTyg(ddate, iid) }, 1000);
    }
}

function GetCountDni(ddate, iid) {
    var dateNow = new Date();	//grab current date
    var amount = ddate.getTime() - dateNow.getTime();	//calc milliseconds between dates
    delete dateNow;
    // if time is already past
    if (amount < 0) {
        document.getElementById(iid).innerHTML = "";
    }
    // else date is still good
    else {
        var days = 0;
        var out = "";
        amount = Math.floor(amount / 1000);//kill the "milliseconds" so just secs
        days = Math.floor(amount / 86400);//days
        out += days + " " + ((days == 1) ? "dzień" : "dni");
        document.getElementById(iid).innerHTML = out;
        setTimeout(function () { GetCountDni(ddate, iid) }, 1000);
    }
}

function GetCountGodz(ddate, iid) {
    var dateNow = new Date();	//grab current date
    var amount = ddate.getTime() - dateNow.getTime();	//calc milliseconds between dates
    delete dateNow;
    // if time is already past
    if (amount < 0) {
        document.getElementById(iid).innerHTML = "";
    }
    // else date is still good
    else {
        var hours = 0;
        var out = "";
        amount = Math.floor(amount / 1000);//kill the "milliseconds" so just secs
        hours = Math.floor(amount / 3600);//hours
        out += hours + " " + Rzeczownik(hours, "godzinę", "godziny", "godzin");
        document.getElementById(iid).innerHTML = out;
        setTimeout(function () { GetCountGodz(ddate, iid) }, 1000);
    }
}

function GetCountMin(ddate, iid) {
    var dateNow = new Date();	//grab current date
    var amount = ddate.getTime() - dateNow.getTime();	//calc milliseconds between dates
    delete dateNow;
    // if time is already past
    if (amount < 0) {
        document.getElementById(iid).innerHTML = "";
    }
    // else date is still good
    else {
        var mins = 0;
        var out = "";
        amount = Math.floor(amount / 1000);//kill the "milliseconds" so just secs
        mins = Math.floor(amount / 60);//minutes
        out += mins + " " + Rzeczownik(mins, "minutę", "minuty", "minut");
        document.getElementById(iid).innerHTML = out;
        setTimeout(function () { GetCountMin(ddate, iid) }, 1000);
    }
}

function GetCountSec(ddate, iid) {
    var dateNow = new Date();	//grab current date
    var amount = ddate.getTime() - dateNow.getTime();	//calc milliseconds between dates
    delete dateNow;
    // if time is already past
    if (amount < 0) {
        document.getElementById(iid).innerHTML = "";
    }
    // else date is still good
    else {
        var secs = 0;
        var out = "";
        var ms = ("" + ((amount % 1000) + 1000)).substring(1, 4);
        amount = Math.floor(amount / 1000);//kill the "milliseconds" so just secs
        secs = Math.floor(amount);//seconds
        out += secs + ("." + ms) + " " + Rzeczownik(secs, "sekundę", "sekundy", "sekund");
        document.getElementById(iid).innerHTML = out;
        setTimeout(function () { GetCountSec(ddate, iid) }, 25);
    }
}

window.addEventListener('load', function () {
    GetCountTyg(dateFuture1, 'tyg_ferie_2');
    GetCountDni(dateFuture1, 'dni_ferie_2');
    GetCountGodz(dateFuture1, 'godz_ferie_2');
    GetCountMin(dateFuture1, 'min_ferie_2');
    GetCountSec(dateFuture1, 'sec_ferie_2');
});
